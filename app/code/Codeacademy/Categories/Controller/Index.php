<?php

namespace Codeacademy\Categories\Controller;
use \Codeacademy\Products\Model\Collection\Products;

class Index
{
    public function index()
    {
        echo 'Not found from controller';
    }

    public function show($id)
    {
        $productsModel = new Products();
        $productsModel->addCategoryFilter($id);
        $products = $productsModel->getCollection();

        print_r($products);

    }
}