<?php

namespace Codeacademy\Products\Model\Collection;

use \Codeacademy\Framework\Helper\SqlBuilder;
use \Codeacademy\Products\Model\Product;

class Products
{
    private $collection = [];

    public function __construct()
    {
        $this->initCollection();
        return $this;
    }

    public function cleanArray($array)
    {
        $cleanArray = [];
        foreach ($array as $element){
            foreach ($element as $line){
                $cleanArray[] = $line;
            }
        }

        return $cleanArray;
    }

    public function addCategoryFilter($id)
    {
        // SELECT product_id FROM products_categories where category_id = $id
        $db = new SqlBuilder;
        $productIds = $db->select('product_id')->from('products_categories')->where('category_id', $id)->get();
        /**
         * [
         *  0 => [
         *      product_id => 1
         * ],
         * 1 => [
         *      product_id => 2
         * ]
         */
        // [1,2,3,4,5]
        foreach ($this->collection as $product) {
            if (!in_array($product->getId(), $productIds)) {
                unset($this->collection[$product->getId()]);
            }
        }
    }

    // $products->addFilter('price', '17', '<'),
    public function addFilter($filter, $value, $operator)
    {

    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function initCollection()
    {
        $db = new SqlBuilder;
        $productsIds = $db->select('id')->from('products'); // ->where()

        foreach ($productsIds as $id) {
            $this->collection[$id] = new Product($id);
        }

    }

}

// $prroduct->getName()