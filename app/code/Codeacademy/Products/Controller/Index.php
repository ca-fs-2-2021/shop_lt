<?php

namespace Codeacademy\Products\Controller;

use \Codeacademy\Framework\Helper\FormBuilder;
use \Codeacademy\Framework\Helper\Request;
use \Codeacademy\Products\Model\Product;
use \Codeacademy\Framework\Helper\Url;
use \Codeacademy\Framework\Core\Controller;

class Index extends Controller
{
    private $request;

    public function __construct()
    {
        $this->request = new Request();
        parent::__construct('Codeacademy/Products');
    }

    public function index()
    {
        echo 'default Products method';
    }

    // Our product create form
    public function create()
    {

        $formHelper = new FormBuilder('POST', Url::getUrl('products/store'), 'form', 'product-form');
        $formHelper->input('text','name', 'text-field', 'product-name', 'Product Name');
        $formHelper->input('text','sku', 'text-field', 'product-sku', 'Product Sku');
        $formHelper->input('text','price', 'text-field', 'product-price', 'Product Price');
        $formHelper->input('number','qty', 'text-field', 'product-qty', 'qty');
        $formHelper->button('ok', 'create');
        $data['title'] = 'Add New Product !!!';
        $data['form'] = $formHelper->get();
        $this->render('form/create', $data);

    }
    // POST requestus
    public function store()
    {

        $name  = $this->request->getPost('name');
        // sku is uniq
        $sku  = $this->request->getPost('sku');
        $price  = $this->request->getPost('price');
        $qty  = $this->request->getPost('qty');

        $product = new Product();
        $product->setName($name);
        $product->setSku($sku);
        $product->setPrice($price);
        $product->setQty($qty);
        $product->save();



    }

    // Show product in front end for Customer
    public function show()
    {
            echo 'show';
    }
    // Form for product create
    public function edit()
    {

    }
    // POST Updated product  in database
    public function update()
    {

    }

    // POST delete product.
    public function delete()
    {

    }
}