<?php

require_once '../app/config/env.php';
require_once '../vendor/autoload.php';

$request = new \Codeacademy\Framework\Helper\Request;
$routes = new \Codeacademy\Framework\Helper\Routing;



$url = $request->getRoute();
/*
 * $url [
 *  'controller' => 'products',
 *  'method' => 'show',
 *  'param'=> 3
 * ]
 *
 *
 */

$routes->getControllerClass($url);

